// IMPORTS
import {Link, NavLink,} from 'react-router-dom';
import {Button} from 'react-bootstrap';

/* 
LONG METHOD
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
*/
import {Container, Nav, Navbar, NavDropdown} from 'react-bootstrap';

import {useContext} from 'react'; // HOOK  
import UserContext from '../UserContext';

export default function AppNavBar() 
{
	// A context object such as our UserContext can be "opened" with React's useContext hook
	const {user} = useContext(UserContext);

	return (
		<Navbar bg="light" expand="lg">
		     <Container>
		       <Navbar.Brand as = {Link} to= "/" >Zuitt</Navbar.Brand>
		       <Navbar.Toggle aria-controls="basic-navbar-nav" />
		       <Navbar.Collapse id="basic-navbar-nav">
		         

		         <Nav className="ms-auto">
		         	
		         		
	         		<Nav.Link as = {NavLink}  to = "/">Home</Nav.Link>
	         		<Nav.Link as = {NavLink} to = "/courses">Courses</Nav.Link>
	         		
	         		{/*conditional rendering*/}
	         		{(user.id !== null) ?
	         			<>
	         			<Link className= "nav-link" to="/logout"> Logout</Link>

	         			<Button variant="outline-info" disabled>
	         			       {`${user.email}`}
	         			     </Button>
	         			</>

	         			:
	         			<>
	         			<Nav.Link as = {NavLink} to = "/login">Login</Nav.Link>
	         			<Nav.Link as = {NavLink} to = "/register">Register</Nav.Link>
	         			</>
	         		}


	    
		         		
		         	
		         </Nav>
		       </Navbar.Collapse>
		     </Container>
		   </Navbar>	
	);
}