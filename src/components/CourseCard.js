import {Col, Card, Button} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import {Link} from 'react-router-dom'

// Destructuring is done in the parameter to retrieve the courseProp
export default function CourseCard ({courseProp})
{
	//console.log(props.courseProp);
	//console.log (courseProp);
	const {_id, name, description, price} = courseProp;
	/*
		const name = courseProp.name;
		const description, = courseProp.description;
		const price = courseProp.price;
	*/

/*	
	HOOKS
	-Use the STATE HOOK for this component to be able to store its state
	-States are used to keep track of information related to inidvidual components
	
	SYNTAX:
		const [getter, setter] = useState (initialGetterValue)
*/

	
	// when a component mounts,  any associated states will undergo a state change from null to the given initial/default state e.g.  count below goes from "null" to 0

	// const [count, setCount] = useState (0);
	// const [seat, setSeat] = useState (30);

	// using the state hook returns an array with the first element being a value and the second element as a function that's used to change the value of the first element.
	
	//console.log (useState(0));
/*
	-Function that keeps track of the enrolles for a course 
	
	-By default JavaScript is sychronous, as it executes code from the top of the file all the way to the bottom aand will wait for the completion of one expression before it proceeds to the next.
	
	-The setter function  for useStates are asynchronous, allowing it to execute separately from other codes in the program.
	
	-the "setCount" function is being executed while the "console.log" is already being completed, resulting in the console to be behind by one count.
*/

/*	function enroll ()
	{
		if (seat > 0)
		{
			setCount (count + 1);
			setSeat (seat - 1);
			//console.log ('Enrollees: ' + count)
		}
		else
		{
			setCount (count);
			setSeat (seat);
			alert("No more seats!")
		}
	}
*/
	// function enroll ()
	// 	{
	// 		setCount (count + 1);
	// 		setSeat (seat - 1);
	// 	}


	/*
	1. useEffect makes any given code block happen when a state changes AND when a component first mounts (initial page load)

	2. in the example below, since count ans seats are in the array of our useState, the code block will execute whenever those state changes

	3. if the array is blank, the code will be executed on component mount ONLY

	4. do NOT omit the array completely 
	*/
		// useEffect (() =>
		// {
		// 	if (seat === 0)
		// 	{
		// 		alert ("No more seats available!")
		// 	}
		// },
		// [count ,seat])
		/*	SYNTAX:
			useEffect (() =>
			{
				<code to be executed>,
				[<state/s> (to be monitored)]
			})
			
		*/

	/*
		si getter naman is to view yung value nung use state po natin, while si setter natin is to change po yung initial value sa may loob nung useState() natin
		-----------------
		si onClick (and other event handler) for reactJS sya, kasi we are using JSX format. Bale ganyan talaga format nya
		---------
		si console.log() synchronous sya, kaya it will print kung anuman yung laman ni count agad.
		dagdagnotes po knina bka di nyo n copy

	*/



	return (
			<Col>
				<Card className = "courseCard my-2">
					<Card.Body>
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle className = "mt-3"> Description: </Card.Subtitle>
						<Card.Text>{description}</Card.Text>

						<Card.Subtitle className = "mt-3">Price:</Card.Subtitle>
						<Card.Text>PhP {price}</Card.Text>
						
						{/*<Card.Subtitle className = "mt-3">Enrollees:</Card.Subtitle>*/}
						{/*<Card.Text>{count} Enrollees </Card.Text>
						<Card.Text>{seat} Seats left </Card.Text>
						<Button variant= "primary" 
						onClick ={enroll}>
       					 Enroll</Button>*/}
       					 <Link className= "btn btn-primary" to= {`courses/${_id}`} >Details</Link>
					</Card.Body>
					
					
				</Card>

			</Col>
		)
}