import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import {Redirect} from 'react-router-dom';


export default function Login (props)
{
	const [email, setEmail] = useState ("");
	const [password, setPassword] = useState ("");
	const [isActive, setIsActive] = useState (false);

	const {user, setUser} = useContext(UserContext);

	useEffect (() =>
	{
		if (email !== "" && password !== "")
		{
			setIsActive (true)
		}
		else
		{
			setIsActive (false)
		}

	}, [email, password])


	const retrieveUserDetails = (token) => 
	{
		fetch(`${process.env.REACT_APP_API_URL}/users/details`,
		{
			method: "POST",
			headers: 
			{
				Authorization : `Bearer ${token}`
			}
		})
		.then (res => res.json())
		.then ( data => {
			// setUser to these values
			setUser (
			{
				id: data._id,
				isAdmin: data.isAdmin,
				email: data.email
			})
		})
	}

	function loginUser (e)
	{
		e.preventDefault() // prevent default form behaviour


		// setItem : allows us to save a key-value pair SYNTAX: (key, value)
		
		// COMMENTED OUT FOR ACTIVITY s55
			/*localStorage.setItem('email' , email)

			alert (`You are now logged in as ${email}!`)
			setUser (
			{
				email: email
			})
			//console.log (email)
			setEmail("")
			setPassword("")*/
		

		/*setUser(
			{email: localStorage.getItem("email")
		}) or*/

		/* ACTIVITY
			1. Create a fetch request inside this function to allow users to login. 
			2. Log in the console the response (access token)
		*/

		// ACTIVITY s55
		fetch (`${process.env.REACT_APP_API_URL}/users/login` , 
		{
			method: "POST",
			headers:
			{
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then (res => res.json())
		.then (data =>
		{
			//console.log(data)

			if (typeof data.access !== "undefined")
			{
				localStorage.setItem("token", data.access)
				// call retrieveUserDetails function and pass JWT to it
				retrieveUserDetails(data.access)

				alert (`Successfully logged in as ${email}`)
				props.history.push("/courses")
			}
			else
			{
				alert ("Login Failed. Please try again")
				setEmail ("");
				setPassword (""); 
			}
		})
	}


	
	
	return ( (user.id !== null) ?
		<Redirect to= "/" />

		:

		<Form onSubmit={e => loginUser(e)} >
			<h3 className="mt-3">User Login Page</h3>

			<Form.Group controlId= "userEmail">
				<Form.Label className="mt-3">Email Address</Form.Label>
				<Form.Control
					type= "email"
					placeholder= "Enter email"
					onChange= {e => setEmail(e.target.value)}
					value= {email}					
					required
				/>
			</Form.Group>

			<Form.Group controlId= "password">
				<Form.Label className="mt-3">Password</Form.Label>
				<Form.Control
					type= "password"
					placeholder= "Enter password"
					onChange= {e => setPassword(e.target.value)}
					value = {password}
					required
				/>
			</Form.Group>

			
			{	isActive ? 
					<Button className="mt-3" variant= "primary" type= "submit" id="submitBtn">Submit</Button>
					:
					<Button className="mt-3" variant= "dark"  id="submitBtn" disabled>Submit</Button>
			}
		</Form>

		)
	


	/*
	ALTERNATE SOLUTION (using if else statements)
	if (user.email !== null)
	{
		return(
		<Redirect to= "/" />
		)
	}
	else
	{
		return (
			<Form onSubmit={e => loginUser(e)} >
				<h3 className="mt-3">User Login Page</h3>

				<Form.Group controlId= "userEmail">
					<Form.Label className="mt-3">Email Address</Form.Label>
					<Form.Control
						type= "email"
						placeholder= "Enter email"
						onChange= {e => setEmail(e.target.value)}
						value= {email}					
						required
					/>
				</Form.Group>

				<Form.Group controlId= "password">
					<Form.Label className="mt-3">Password</Form.Label>
					<Form.Control
						type= "password"
						placeholder= "Enter password"
						onChange= {e => setPassword(e.target.value)}
						value = {password}
						required
					/>
				</Form.Group>

				
				{	isActive ? 
						<Button className="mt-3" variant= "primary" type= "submit" id="submitBtn">Submit</Button>
						:
						<Button className="mt-3" variant= "dark"  id="submitBtn" disabled>Submit</Button>
				}

				
			</Form>
			)
	}
*/
	

}