// IMPORTS
import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import {Redirect} from 'react-router-dom';


export default function Register (props)
{
	//console.log(props)

	const [firstName, setFirstName] = useState ("");
	const [lastName, setLastName] = useState ("");
	const [email, setEmail] = useState ("");
	const [mobileNo, setMobileNo] = useState ("");
	const [password1, setPassword1] = useState ("");
	const [password2, setPassword2] = useState ("");
	const [isActive, setIsActive] = useState (false);

	const {user, setUser} = useContext(UserContext);

	/*
		To properly change and input values, we must implement two-way binding. (value & onChange)

		We need to capture whatever the user types in the input as they are typing

		Meaning we need the input's .value

		To get the .value, we need to capture the event (in this case, onChange). The target of the onChange is the input, meaning we can get the .value
	*/

	/*mini-project change password visibility*/
	
	/* USE EFFECT (useEffect)

		Syntax:
			useEffect (() =>
			{
				<code to be executed>,
				[<state/s> (to be monitored)]
			})

	*/

		useEffect (() =>
		{
			// console.log (email)
			// console.log (password1)
			// console.log (password2)

			if ((firstName !== "" && lastName !== "" &&  email !== "" && mobileNo.length === 11 && password1 !== "" && password2 !== "" ) && (password1 === password2))
			{
				setIsActive (true)
			}
			else
			{
				setIsActive (false)
			}

		}, [firstName, lastName, email, mobileNo, password1, password2])


	function registerUser (e)
	{
		e.preventDefault() // prevent default form behaviour
		//alert (`Thanks for registering ${email}!`)
		
		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, 
		{
			method: "POST",
			headers: 
			{
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email
			})	
		})

		.then (res => res.json())
		.then (data => 
		{
			if (data)
			{
				alert ("Duplicate email exists")
			}
			else
			{
				fetch(`${process.env.REACT_APP_API_URL}/users/register`,
				{
					method: "POST",
					headers: 
					{
						'Content-Type' : 'application/json'
					},
					body: JSON.stringify({
						firstName : firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1
					})
				})
				.then(res => res.json())
				.then (data =>
				{
					if (data)
					{
						alert (`Thanks for registering ${firstName}!`)

						//redirects the user to the login page
						props.history.push("/login")
					}
					else
					{
						alert ("Something went wrong")
					}
				})
			}
		})
	}

	return ( (user.id !== null) ?
		<Redirect to='/' />
		:
		<Form onSubmit={e => registerUser(e)} >
			<Form.Group controlId= "firstName">
				<Form.Label className="mt-3">First Name</Form.Label>
				<Form.Control
					type= "text"
					placeholder= "Enter first name"
					onChange= {e => setFirstName(e.target.value)}
					value= {firstName}					
					required
				/>
			</Form.Group>

			<Form.Group controlId= "lastName">
				<Form.Label className="mt-3">Last Name</Form.Label>
				<Form.Control
					type= "text"
					placeholder= "Enter last name"
					onChange= {e => setLastName(e.target.value)}
					value= {lastName}					
					required
				/>
			</Form.Group>

			<Form.Group controlId= "userEmail">
				<Form.Label className="mt-3">Email Address</Form.Label>
				<Form.Control
					type= "email"
					placeholder= "Enter email"
					onChange= {e => setEmail(e.target.value)}
					value= {email}					
					required
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else
				</Form.Text>
			</Form.Group>

			<Form.Group controlId= "mobileNo">
				<Form.Label className="mt-3">Mobile Number</Form.Label>
				<Form.Control
					type= "text"
					placeholder= "09XXXXXXXXX"
					onChange= {e => setMobileNo(e.target.value)}
					value= {mobileNo}					
					required
				/>
			</Form.Group>

			<Form.Group controlId= "password1">
				<Form.Label className="mt-3">Password</Form.Label>
				<Form.Control
					type= "password"
					placeholder= "Enter password"
					onChange= {e => setPassword1(e.target.value)}
					value = {password1}
					required
				/>
			</Form.Group>

			<Form.Group controlId= "password2">
				<Form.Label className="mt-3">Verify Password</Form.Label>
				<Form.Control
					type= "password"
					placeholder= "Verify password"
					onChange= {e => setPassword2(e.target.value)}
					value = {password2}
					required
				/>
			</Form.Group>

			{	isActive ? 
					<Button className="mt-3" variant= "primary" type= "submit" id="submitBtn">Submit</Button>
					:
					<Button className="mt-3" variant= "dark"  id="submitBtn" disabled>Submit</Button>
			}

			
		</Form>
		)
}