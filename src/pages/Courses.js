// IMPORT 
	import {useEffect, useState, useContext} from 'react';
	//import courseData from '../data/courseData' ;
	import CourseCard from '../components/CourseCard';
	import AdminView from '../components/AdminView';
	import UserContext from '../UserContext'
	

// EXPORT
	export default function Courses()
	{
		const [coursesData, setCoursesData] = useState ([]);

		const {user} = useContext(UserContext)
		//console.log (user)

		//console.log(courseData[0]);

		/*
			PROPS
				- shorthand for "property" since components are considered as object in ReactJS
				- Props is a way to pass data from a parent component to a child component.
				- it is synonymous to the function parameter.
				- This is reffered to as "props drilling"
		*/

		// .map will generate a new array so in this case, courses becomes an array of objects


		const fetchData = () => 
		{
			fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
			.then (res => res.json())
			.then (data => 
			{
				console.log(data)
				setCoursesData(data)
			})
		}

		// Fetch by default always makes a GET request, unless a different one is specified
		
		// ALWAYS add fetch request for getting data in a useEffect hook
		useEffect(() =>
		{
			console.log(process.env.REACT_APP_API_URL)
			// changes to env files are applied only at build time (when starting the project locally)
			fetchData()
			
		}, [])

		const courses = coursesData.map(course =>
		{
			if (course.isActive)
			{
				return (
					<CourseCard courseProp = {course} key={course._id} />
					)
			}
			else
			{
				return null
			}
		})

		/* PREVIOUS VERSION
		const courses = courseData.map(course =>
		{
			return (
				<CourseCard courseProp = {course} key={course.id} />
				)
		})
		*/
		return ( (user.isAdmin) ?
			<AdminView coursesProp={coursesData} fetchData={fetchData} />
			:
			<>
				<h1>Courses</h1>
				{courses}
				
			</>
				
			)
	}

	
	/*
		EXPERIMENT
			let name = "Raf"
			const courses = courseData.map(course =>
			{
				return (
					<CourseCard courseProp = {course} name = {} key={course.id} />
					)
			})
			return (
				<>
					<h1>Courses</h1>
					{courses}
					
				</>
					
				)
		}

	*/