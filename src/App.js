// IMPORTS:
//import logo from './logo.svg';
import './App.css';
import AppNavBar from './components/AppNavBar';
import Courses from './pages/Courses' ;
import SpecificCourse from './pages/SpecificCourse'
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';

import {UserProvider} from './UserContext';

import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'


        // the as keyword gives an alias to the component upon import

/*
    All other components/pages will be contained in our main component: <App />
  
    Fragment:
    <>
    ....  
    </>
      ensure that adjacent JS elements will be rendered and avoid error

*/

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
    email: null
  })

  // since reloading the page resets our states' properties to null, we need to re-retrieve the properties from our API

  // to do so, we run useEffect with a fetch request then re-set the user's states
  useEffect (() => 
  {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`,
    {
      method: "POST",
      headers: 
      {
        Authorization : `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then (res => res.json())
    .then ( data => {
      // setUser to these values
      setUser (
      {
        id: data._id,
        isAdmin: data.isAdmin,
        email: data.email
      })
    })
  }, [])


  return (
    // The UserProvider component has a value attribute that we can use to pass our user state to our components
    <UserProvider value={{user, setUser}} >
      <Router>
        <>
        <AppNavBar />
        <Container>
            <Switch>
              <Route exact path = "/" component = {Home}/>
              <Route exact path = "/register" component = {Register}/>
              <Route exact path = "/login" component = {Login}/>
              <Route exact path = "/logout" component = {Logout}/>
              <Route exact path = "/courses" component = {Courses}/>
              <Route exact path = "/courses/:courseId" component = {SpecificCourse}/>
              <Route component = {Error}/>
            </Switch>  
        </Container> 
        </>    
      </Router>
    </UserProvider>
  );
}

export default App;



/*
    
    <Routes>
      <Route exact path="/" element= {<Home/>} />
      <Route exact path="/courses" element= {<Courses/>} />
      <Route exact path="/register" element= {<Register/>} />
      <Route exact path="/login" element= {<Login/>} />
      <Route element= {Error} />
    </Routes>
*/